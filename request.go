package httpreq

func NewRequest(method, url string, payload interface{}, headers ...string) (response Response, err error) {
	var req Request
	req.Init(method, url, payload, headers...)
	return req.Do()
}
