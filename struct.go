package httpreq

import (
	"bytes"
	"gitlab.com/modules-shortcut/go-utils"
	"io/ioutil"
	"net/http"
	"strings"
)

type Request struct {
	method  string
	url_    string
	header  http.Header
	payload interface{}
}

type Response struct {
	Body       []byte
	Status     string
	StatusCode int
}

func (this *Request) SetUrl(url_ string) {
	this.url_ = url_
}
func (this *Request) SetMethod(method string) {
	this.method = method
}
func (this *Request) SetHeader(headers ...string) {
	if len(headers) > 1 {
		this.header = make(http.Header)
		for id := range headers {
			if id%2 == 1 {
				this.header.Set(headers[id-1], headers[id])
			}
		}
	}
}
func (this *Request) SetPayload(payload interface{}) {
	this.payload = payload
}
func (this *Request) Init(method, url_ string, payload interface{}, headers ...string) {
	this.SetMethod(method)
	this.SetUrl(url_)
	this.SetHeader(headers...)
	this.SetPayload(payload)
}
func (this *Request) Do() (response Response, err error) {
	var (
		client = &http.Client{}
		req    *http.Request
		res    *http.Response
	)

	if this.payload != nil {
		if val, ok := this.payload.(*strings.Reader); ok {
			req, err = http.NewRequest(this.method, this.url_, val)
		}
		if val, ok := this.payload.(*bytes.Buffer); ok {
			req, err = http.NewRequest(this.method, this.url_, val)
		}
	} else {
		req, err = http.NewRequest(this.method, this.url_, nil)
	}

	if err != nil {
		utils.ErrorHandler(err)
		return
	}

	req.Header = this.header
	if res, err = client.Do(req); err != nil {
		utils.ErrorHandler(err)
		return
	}
	defer utils.CloseBody(res.Body)

	response.Status = res.Status
	response.StatusCode = res.StatusCode
	response.Body, err = ioutil.ReadAll(res.Body)
	return
}
